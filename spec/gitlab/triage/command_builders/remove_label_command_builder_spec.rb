# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/command_builders/remove_label_command_builder'

describe Gitlab::Triage::CommandBuilders::RemoveLabelCommandBuilder do
  it_behaves_like 'label command', 'unlabel'
end
