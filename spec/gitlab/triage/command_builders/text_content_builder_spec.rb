# frozen_string_literal: true

require 'spec_helper'

require 'active_support/all'
require 'gitlab/triage/command_builders/text_content_builder'

describe Gitlab::Triage::CommandBuilders::TextContentBuilder do
  let(:items) do
    [
      "comment",
      "another comment"
    ]
  end

  describe '#build_command' do
    it 'outputs the correct command' do
      builder = described_class.new(items)
      expect(builder.build_command).to eq("comment\n\nanother comment")
    end

    it 'outputs an empty string if no items present' do
      builder = described_class.new([])
      expect(builder.build_command).to eq("")

      builder = described_class.new(nil)
      expect(builder.build_command).to eq("")
    end

    describe 'placeholders' do
      let(:builder) { described_class.new(lines, resource: resource) }

      let(:expected_confidential_text) do
        lines.sub(/\{\{.+\}\}/, '(confidential)')
      end

      shared_examples 'placeholder subsitution' do
        it 'replaces the placeholder with the expected values' do
          expect(builder.build_command).to eq(expected_text)
        end

        it 'does not replace the original item' do
          original_lines = lines.deep_dup

          builder.build_command

          expect(lines).to eq(original_lines)
        end

        context 'when the resource is confidential' do
          before do
            resource[:confidential] = true
          end

          it 'replaces the placeholder with (confidential)' do
            expect(builder.build_command).to eq(expected_confidential_text)
          end

          context 'when we disable redaction' do
            let(:builder) do
              described_class.new(
                lines,
                resource: resource,
                redact_confidentials: false)
            end

            it 'replaces the placeholder with the expected values' do
              expect(builder.build_command).to eq(expected_text)
            end
          end
        end
      end

      context 'when field value is nil' do
        let(:resource) { {} }
        let(:lines) { 'Hey {{assignee}}, please take a look!' }

        it 'replaces with an empty string' do
          expect(builder.build_command).to eq('Hey , please take a look!')
        end
      end

      context 'when there is not such field' do
        let(:resource) { {} }
        let(:lines) { 'Hey {{assigner}}, please take a look!' }

        it 'keeps the unknown placeholder' do
          expect(builder.build_command)
            .to eq('Hey {{assigner}}, please take a look!')
        end
      end

      describe '{{created_at}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { created_at: '2018-06-21T08:16:14.018Z' } }
          let(:lines) { "This resource's created_at is {{created_at}}!" }
          let(:expected_text) { "This resource's created_at is 2018-06-21T08:16:14.018Z!" }
        end
      end

      describe '{{updated_at}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { updated_at: '2018-06-21T08:16:14.018Z' } }
          let(:lines) { "This resource's updated_at is {{updated_at}}!" }
          let(:expected_text) { "This resource's updated_at is 2018-06-21T08:16:14.018Z!" }
        end
      end

      describe '{{merged_at}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { merged_at: '2018-06-21T08:16:14.018Z' } }
          let(:lines) { "This resource's merged_at is {{merged_at}}!" }
          let(:expected_text) { "This resource's merged_at is 2018-06-21T08:16:14.018Z!" }
        end
      end

      describe '{{closed_at}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { closed_at: '2018-06-21T08:16:14.018Z' } }
          let(:lines) { "This resource's closed_at is {{closed_at}}!" }
          let(:expected_text) { "This resource's closed_at is 2018-06-21T08:16:14.018Z!" }
        end
      end

      describe '{{state}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { state: 'closed' } }
          let(:lines) { 'This resource is {{state}}!' }
          let(:expected_text) { "This resource is closed!" }
        end
      end

      describe '{{author}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { author: { username: 'user1' } } }
          let(:lines) { 'Hey {{author}}, please take a look!' }
          let(:expected_text) { "Hey @user1, please take a look!" }
          let(:expected_confidential_text) do
            "Hey @(confidential), please take a look!"
          end
        end
      end

      describe '{{assignee}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { assignee: { username: 'user2' } } }
          let(:lines) { 'Hey {{assignee}}, please take a look!' }
          let(:expected_text) { "Hey @user2, please take a look!" }
          let(:expected_confidential_text) do
            "Hey @(confidential), please take a look!"
          end
        end
      end

      describe '{{assignees}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { assignees: [{ username: 'user2' }, { username: 'user3' }] } }
          let(:lines) { 'Hey {{assignees}}, please take a look!' }
          let(:expected_text) { "Hey @user2, @user3, please take a look!" }
          let(:expected_confidential_text) do
            "Hey @(confidential), please take a look!"
          end
        end
      end

      describe '{{assignees}} with 1 user placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { assignees: [{ username: 'user4' }] } }
          let(:lines) { 'Hey {{assignees}}, please take a look!' }
          let(:expected_text) { "Hey @user4, please take a look!" }
          let(:expected_confidential_text) do
            "Hey @(confidential), please take a look!"
          end
        end
      end

      describe '{{reviewers}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { reviewers: [{ username: 'user2' }, { username: 'user3' }] } }
          let(:lines) { 'Hey {{reviewers}}, please take a look!' }
          let(:expected_text) { "Hey @user2, @user3, please take a look!" }
          let(:expected_confidential_text) do
            "Hey @(confidential), please take a look!"
          end
        end
      end

      describe '{{reviewers}} with 1 user placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { reviewers: [{ username: 'user4' }] } }
          let(:lines) { 'Hey {{reviewers}}, please take a look!' }
          let(:expected_text) { "Hey @user4, please take a look!" }
          let(:expected_confidential_text) do
            "Hey @(confidential), please take a look!"
          end
        end
      end

      describe '{{closed_by}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { closed_by: { username: 'user1' } } }
          let(:lines) { 'Hey {{closed_by}}, please take a look!' }
          let(:expected_text) { "Hey @user1, please take a look!" }
          let(:expected_confidential_text) do
            "Hey @(confidential), please take a look!"
          end
        end
      end

      describe '{{merged_by}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { merged_by: { username: 'user1' } } }
          let(:lines) { 'Hey {{merged_by}}, please take a look!' }
          let(:expected_text) { "Hey @user1, please take a look!" }
          let(:expected_confidential_text) do
            "Hey @(confidential), please take a look!"
          end
        end
      end

      describe '{{milestone}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { milestone: { title: "11.1" } } }
          let(:lines) { 'This resource has the {{milestone}} milestone!' }
          let(:expected_text) { h('This resource has the %"11.1" milestone!') }
          let(:expected_confidential_text) do
            h('This resource has the %"(confidential)" milestone!')
          end
        end
      end

      describe '{{labels}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { labels: ['In dev', 'frontend'] } }
          let(:lines) { 'These {{labels}} are awesome!' }
          let(:expected_text) { h('These ~"In dev", ~"frontend" are awesome!') }
          let(:expected_confidential_text) do
            h('These ~"(confidential)" are awesome!')
          end
        end
      end

      describe '{{upvotes}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { upvotes: 42 } }
          let(:lines) { "This resource has {{upvotes}} upvotes!" }
          let(:expected_text) { "This resource has 42 upvotes!" }
        end
      end

      describe '{{downvotes}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { downvotes: 42 } }
          let(:lines) { "This resource has {{downvotes}} downvotes!" }
          let(:expected_text) { "This resource has 42 downvotes!" }
        end
      end

      describe '{{title}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { title: 'Awesome title' } }
          let(:lines) { "Title is {{title}}!" }
          let(:expected_text) { "Title is Awesome title!" }
        end

        context 'when title contains HTML' do
          it_behaves_like 'placeholder subsitution' do
            let(:resource) { { title: 'Awesome <img> tag' } }
            let(:lines) { "Title is {{title}}!" }
            let(:expected_text) { h("Title is Awesome <img> tag!") }
          end
        end
      end

      describe '{{web_url}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { web_url: 'http://example.com' } }
          let(:lines) { "Link is {{web_url}}" }
          let(:expected_text) { "Link is http://example.com" }
          let(:expected_confidential_text) { expected_text }
        end
      end

      describe '{{full_reference}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { references: { full: 'gitlab-org/gitlab!42' } } }
          let(:lines) { "Here is a reference: {{full_reference}}" }
          let(:expected_text) { "Here is a reference: gitlab-org/gitlab!42" }
          let(:expected_confidential_text) { expected_text }
        end
      end

      describe '{{type}} placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { type: 'issues' } }
          let(:lines) { "This is for {{type}}" }
          let(:expected_text) { "This is for issues" }
          let(:expected_confidential_text) { expected_text }
        end
      end

      # rubocop:disable Lint/InterpolationCheck
      describe '#{1 + 1} interpolation' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { {} }
          let(:lines) { 'I know 1 + 1 = #{1 + 1}!' }
          let(:expected_text) { 'I know 1 + 1 = 2!' }
          let(:expected_confidential_text) { expected_text }
        end
      end

      describe '#{milestone.title} interpolation' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { milestone: { title: '11.3' }, type: 'issues' } }
          let(:lines) { 'Please add ~"missed:#{milestone.title}" label' }
          let(:expected_text) { 'Please add ~"missed:11.3" label' }
          let(:expected_confidential_text) do
            'Please add ~"missed:(confidential)" label'
          end
        end
      end

      describe '#{"{{state}}"} interpolation as placeholder' do
        it_behaves_like 'placeholder subsitution' do
          let(:resource) { { state: 'open' } }
          let(:lines) { 'This item is #{"{{state}}"}' }
          let(:expected_text) { 'This item is open' }
          let(:expected_confidential_text) { 'This item is (confidential)' }
        end
      end
      # rubocop:enable Lint/InterpolationCheck
    end
  end
end
