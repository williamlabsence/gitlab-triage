# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/command_builders/move_command_builder'

describe Gitlab::Triage::CommandBuilders::MoveCommandBuilder do
  let(:destination) { 'path/to/project' }

  describe '#build_command' do
    it 'outputs the correct command' do
      builder = described_class.new(destination)
      expect(builder.build_command).to eq("/move path/to/project")
    end

    it 'outputs an empty string if command not present' do
      builder = described_class.new(nil)
      expect(builder.build_command).to eq("")
    end

    it 'outputs an empty string if command empty' do
      builder = described_class.new("")
      expect(builder.build_command).to eq("")
    end
  end
end
