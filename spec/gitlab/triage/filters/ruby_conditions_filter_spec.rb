# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/filters/ruby_conditions_filter'
require 'gitlab/triage/network'
require 'gitlab/triage/network_adapters/httparty_adapter'

describe Gitlab::Triage::Filters::RubyConditionsFilter do
  include_context 'with network context'

  let(:resource) do
    { milestone: milestones.first, type: 'issues' }
  end

  let(:milestones) do
    [
      {
        id: 1,
        project_id: 2,
        start_date: '2018-01-01'
      },
      {
        id: 3,
        project_id: 2,
        start_date: '2018-02-01'
      }
    ]
  end

  let(:condition) do
    'Date.today > milestone.succ.start_date'
  end

  subject { described_class.new(resource, condition, network) }

  before do
    allow(network).to receive(:print)
  end

  it_behaves_like 'a filter'

  describe '#calculate' do
    it 'evaluates the expression properly' do
      stub_api(
        :get, 'http://test.com/api/v4/projects/2/milestones',
        query: { per_page: 100, state: 'active' }) { milestones }

      expect(subject.calculate).to be_truthy
    end

    context 'when there is an internal error' do
      before do
        i = instance_double(Gitlab::Triage::Resource::Issue)
        allow(Gitlab::Triage::Resource::Issue).to receive(:new).and_return(i)
        allow(i).to receive(:milestone).and_raise(NameError)
      end

      it 'captures the backtrace pointing to where the actual error raised' do
        expect { subject.calculate }.to raise_error(NameError)
      end
    end

    context 'when the resource is confidential' do
      let(:resource) do
        { confidential: true, labels: %w[bug regression], type: 'issues' }
      end

      let(:condition) do
        'labels.map(&:name).include?("regression")'
      end

      it 'evaluates the expression properly' do
        expect(subject.calculate).to be_truthy
      end
    end
  end
end
