# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/filters/author_member_conditions_filter'

describe Gitlab::Triage::Filters::AuthorMemberConditionsFilter do
  let(:type) { :author }

  it_behaves_like 'a member filter'
end
