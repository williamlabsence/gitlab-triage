# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/expand_condition/sequence'

describe Gitlab::Triage::ExpandCondition::Sequence do
  describe '.expand' do
    it 'expands the conditions for labels' do
      conditions = subject.expand(
        labels: %w[missed:{0..1} missed:{2..3} missed]
      )

      expect(conditions).to eq(
        [
          { labels: %w[missed:0 missed:2 missed] },
          { labels: %w[missed:0 missed:3 missed] },
          { labels: %w[missed:1 missed:2 missed] },
          { labels: %w[missed:1 missed:3 missed] }
        ]
      )
    end

    it 'expands the conditions for labels, ignoring spaces' do
      conditions = subject.expand(
        labels: ['P{ 1 .. 4 }']
      )

      expect(conditions).to eq(
        [
          { labels: %w[P1] },
          { labels: %w[P2] },
          { labels: %w[P3] },
          { labels: %w[P4] }
        ]
      )
    end
  end
end
