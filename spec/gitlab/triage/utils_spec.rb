# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/utils'

describe Gitlab::Triage::Utils do
  describe '.graphql_quote' do
    it { expect(described_class.graphql_quote('string')).to eq('"string"') }
    it { expect(described_class.graphql_quote('str"i"ng')).to eq('"str\\"i\\"ng"') }
  end
end
