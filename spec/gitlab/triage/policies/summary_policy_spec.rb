# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/policies/summary_policy'
require 'gitlab/triage/policies_resources/summary_resources'
require 'gitlab/triage/policies_resources/rule_resources'

describe Gitlab::Triage::Policies::SummaryPolicy do
  include_context 'with network context'

  let(:type) { 'issues' }
  let(:name) { 'Policy name' }

  let(:actions) { { summarize: { summary: 'Grand summary: {{items}}' } } }
  let(:policy_spec) { { name: name, actions: actions } }

  let(:rule_a) do
    { name: 'Name A', actions: { summarize: { summary: 'Summary A' } } }
  end

  let(:rule_b) do
    { name: 'Name B', actions: { summarize: { summary: 'Summary B' } } }
  end

  let(:resources) do
    Gitlab::Triage::PoliciesResources::SummaryResources.new(
      rule_a => Gitlab::Triage::PoliciesResources::RuleResources.new(
        [{ title: name }]
      ),
      rule_b => Gitlab::Triage::PoliciesResources::RuleResources.new(
        []
      )
    )
  end

  let(:expected_resources) do
    {
      rule_a => [{ title: name, type: type }],
      rule_b => []
    }
  end

  subject { described_class.new(type, policy_spec, resources, network) }

  describe '#build_summary' do
    it 'contains the summary with resources but not without' do
      issue = subject.build_summary

      expect(issue.description).to include('Grand summary')
      expect(issue.description).to include('Summary A')
      expect(issue.description).not_to include('Summary B')
    end
  end
end
