# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/policies/base_policy'

describe Gitlab::Triage::Policies::BasePolicy do
  include_context 'with network context'

  let(:type) { 'issues' }
  let(:name) { 'Policy name' }
  let(:actions_hash) { {} }
  let(:policy_spec) { { name: name, actions: actions_hash } }
  let(:resources) { [] }

  subject { described_class.new(type, policy_spec, resources, network) }

  describe 'validate!' do
    context 'when there is a comment_on_summary and a summarize action' do
      let(:actions_hash) { { summarize: {}, comment_on_summary: 'foo' } }

      it 'does not raise errors' do
        expect { subject.validate! }.not_to raise_error
      end
    end

    context 'when there is a comment_on_summary but no summarize' do
      let(:actions_hash) { { comment_on_summary: 'foo' } }

      it 'raises a validation error' do
        expect { subject.validate! }.to raise_error(described_class::InvalidPolicyError)
      end
    end
  end

  describe '#type' do
    it 'returns the type' do
      expect(subject.type).to eq(type)
    end
  end

  describe '#policy_spec' do
    it 'returns the policy_spec' do
      expect(subject.policy_spec).to eq(policy_spec)
    end
  end

  describe '#network' do
    it 'returns the network' do
      expect(subject.network).to eq(network)
    end
  end

  describe '#name' do
    context 'when the policy has no name' do
      let(:name) { nil }

      it 'generates a unique name' do
        expect(subject.name).to match(/\A#{type}-\w+\z/)
      end
    end

    context 'when the policy has a name' do
      it 'generates a unique name' do
        expect(subject.name).to eq(name)
      end
    end
  end

  describe '#source' do
    context 'when the policy type is "epics"' do
      let(:type) { 'epics' }

      it { expect(subject.source).to eq('groups') }
    end

    context 'when the policy type is "issues"' do
      let(:type) { 'issues' }

      it { expect(subject.source).to eq('projects') }
    end

    context 'when the policy type is "merge_requests"' do
      let(:type) { 'merge_requests' }

      it { expect(subject.source).to eq('projects') }
    end
  end

  describe '#source_id_sym' do
    context 'when the policy type is "epics"' do
      let(:type) { 'epics' }

      it { expect(subject.source_id_sym).to eq(:group_id) }
    end

    context 'when the policy type is "issues"' do
      let(:type) { 'issues' }

      it { expect(subject.source_id_sym).to eq(:project_id) }
    end

    context 'when the policy type is "merge_requests"' do
      let(:type) { 'merge_requests' }

      it { expect(subject.source_id_sym).to eq(:project_id) }
    end
  end

  describe '#actions' do
    it 'returns the actions hash' do
      expect(subject.actions).to eq(actions_hash)
    end
  end

  describe '#summarize?' do
    context 'when the actions hash does not have the :summarize key' do
      it 'returns false' do
        expect(subject).not_to be_summarize
      end
    end

    context 'when the actions hash has the :summarize key' do
      let(:actions_hash) { { summarize: {} } }

      it 'returns true' do
        expect(subject).to be_summarize
      end
    end
  end

  describe '#comment?' do
    context 'when the actions hash does not have any other keys than the :summarize key' do
      let(:actions_hash) { { summarize: {} } }

      it 'returns false' do
        expect(subject).not_to be_comment
      end
    end

    context 'when the actions hash has other keys than the :summarize key' do
      let(:actions_hash) { { summarize: {}, mention: [] } }

      it 'returns true' do
        expect(subject).to be_comment
      end
    end
  end

  describe '#build_issue' do
    it 'raises a NotImplementedError error' do
      expect { subject.build_issue }.to raise_error(NotImplementedError)
    end
  end

  describe '#build_summary' do
    it 'raises a NotImplementedError error' do
      expect { subject.build_summary }.to raise_error(NotImplementedError)
    end
  end
end
