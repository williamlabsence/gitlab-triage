# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/resource/instance_version'

describe Gitlab::Triage::Resource::InstanceVersion do
  include_context 'with network context'

  subject { described_class.new(network: network) }

  let(:version) { '11.3.0-rc11-ee' }
  let(:revision) { '231b0c7' }

  before do
    stub_api(:get, 'http://test.com/api/v4/version') do
      {
        version: version,
        revision: revision
      }
    end
  end

  describe '#version' do
    it 'returns the full version string' do
      expect(subject.version).to eq(version)
    end
  end

  describe '#version_short' do
    it 'returns the short version string' do
      expect(subject.version_short).to eq('11.3')
    end
  end

  describe '#revision' do
    it 'returns the revision string' do
      expect(subject.revision).to eq(revision)
    end
  end
end
