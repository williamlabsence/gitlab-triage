stages:
  - prepare
  - test
  - triage
  - deploy

default:
  image: ruby:3.2
  tags:
    - gitlab-org
  cache:
    key:
      files:
        - Gemfile
        - gitlab-triage.gemspec
    paths:
      - vendor/ruby
      - Gemfile.lock
    policy: pull
  before_script:
    - ruby --version
    - gem install bundler --no-document
    - bundle --version
    - bundle install --jobs $(nproc) --path=vendor --retry 3 --quiet
    - bundle check

workflow:
  rules:
    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
    # For `master` branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # For tags, create a pipeline.
    - if: '$CI_COMMIT_TAG'

.use-docker-in-docker:
  image: docker:${DOCKER_VERSION}
  services:
    - docker:${DOCKER_VERSION}-dind
  variables:
    DOCKER_VERSION: "26.0.1"
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
  tags:
    # See https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7019 for tag descriptions
    - gitlab-org-docker

###################
## Prepare stage ##
###################
setup-test-env:
  stage: prepare
  script:
    - echo "Setup done!"
  cache:
    policy: pull-push
  artifacts:
    paths:
      - Gemfile.lock

################
## Test stage ##
################
rubocop:
  stage: test
  needs: ["setup-test-env"]
  dependencies: ["setup-test-env"]
  script:
    - bundle exec rubocop --parallel
  cache:
    key: "$CI_JOB_NAME"
    paths:
      - .cache/rubocop_cache/

specs:
  needs: ["setup-test-env"]
  stage: test
  script:
    - bundle exec rake spec
  image: $RUBY_IMAGE
  parallel:
    matrix:
      - RUBY_IMAGE:
        - ruby:3.0
        - ruby:3.1
        - ruby:3.2
        - ruby:3.3

##################
## Triage stage ##
##################
dry-run:gitlab-triage:
  needs: ["setup-test-env"]
  stage: triage
  script:
    - bundle exec rake build
    - gem install pkg/*.gem
    - which gitlab-triage
    - gitlab-triage --version
    - gitlab-triage --help
    - gitlab-triage --init
    - gitlab-triage --dry-run --debug --source-id $CI_PROJECT_PATH

# This job requires allows to override the `CI_PROJECT_PATH` variable when triggered.
dry-run:custom:
  extends: dry-run:gitlab-triage
  rules:
    - when: manual
      allow_failure: true

include:
  - template: Jobs/Code-Quality.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - project: 'gitlab-org/quality/pipeline-common'
    file:
      - '/ci/danger-review.yml'
      - '/ci/gem-release.yml'

code_quality:
  needs: ["setup-test-env"]
  dependencies: ["setup-test-env"]
  tags:
    - gitlab-org-docker
  before_script: []
  rules:
    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
    # For `master` branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # For tags, create a pipeline.
    - if: '$CI_COMMIT_TAG'

